
//Import React
import React, {useState, useEffect} from "react";

//Import all required component
import {
  PermissionsAndroid,
  Platform,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  FlatList,
  TextInput
} from "react-native";

import Contacts from "react-native-contacts";
import ListItem from "./ListItem";

const ContactHome = () => {
  let [contacts, setContacts] = useState([]);

  useEffect(() => {
    if (Platform.OS === "android") {
      PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
        title: "Contacts",
        message: "This app would like to view your contacts."
      }).then(() => {
        loadContacts();
      });
    } else {
      loadContacts();
    }
  }, []);

  const loadContacts = () => {
    Contacts.getAll((err, contacts) => {
        console.log(contacts.length)
    contacts.sort((A, B) => A.givenName > B.givenName);
    
    //   console.log('contacts -> ', contacts);
      if (err === "denied") {
        alert("Permission to access contacts was denied");
        console.warn("Permission to access contacts was denied");
      } else {
        setContacts(contacts);
        // console.log('contacts', contacts);
      }
    });
  }

  const search = (text) => {
    const phoneNumberRegex = /\b[\+]?[(]?[0-9]{2,6}[)]?[-\s\.]?[-\s\/\.0-9]{3,15}\b/m;
    if (text === "" || text === null) {
      loadContacts();
    } else if (phoneNumberRegex.test(text)) {
      Contacts.getContactsByPhoneNumber(text, (err, contacts) => {
        contacts.sort((a, b) => a.givenName.toLowerCase() > b.givenName.toLowerCase());
        setContacts(contacts);
        // console.log('contacts', contacts);
      });
    } else {
      Contacts.getContactsMatchingString(text, (err, contacts) => {
        contacts.sort((a, b) => a.givenName.toLowerCase() > b.givenName.toLowerCase());
        setContacts(contacts);
        // console.log('contacts', contacts);
      });
    }
  }
// menu buat ke detail kontak
  const openContact = (contact) => {
    console.log('Dari json',JSON.stringify(contact), '\n');
    Contacts.openExistingContact(contact, () => {})
    console.log('/n b aja',contact);
  };
// const KeDetail = (contact) => {
//   this.props.navigation.navigate('DetContact')
// };
  return (
    <SafeAreaView style={styles.container}>
        <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.text1}>
        Contact List
        </Text>
        </View>
        <View>
        <TextInput
          onChangeText={search}
          placeholder='Search'
          style={styles.searchBar}
          
        />
        </View>
        <Text style={styles.searchBar}>Total Kontak:  {contacts.length}</Text>
        <FlatList
          data={contacts}
          renderItem={(contact) => {
            // {console.log('contact -> ' + JSON.stringify(contact))}
         
            return (<ListItem
              key={contact.item.recordID}
              item={contact.item}
              onPress={openContact}
    
            />)
          }}
          keyExtractor={item => item.recordID}
        />
      </View>
    </SafeAreaView>
  );
}
export default ContactHome;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  header: {
    width: '100%',
    height: '8%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00796B'
  },
  searchBar: {
    backgroundColor: '#f0eded',
    paddingHorizontal: 30,
    paddingVertical: (Platform.OS === "android") ? undefined: 15,
  },
  text1: {
    color: '#E0F7FA',
    fontWeight: 'bold',
    fontSize: 35,
    letterSpacing: 0
  },
});
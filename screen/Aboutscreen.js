import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

// Nambah
// end


  
export default class Aboutscreen extends React.Component {
  render() {
    return (
      <>
<View style={styles.header}>
  <Text style={styles.text1}>ABOUT ME</Text>
</View>
<Text style={styles.textB}>Terima kasih sudah menginstall aplikasi ini, Tentunya masih ada banyak kesalahan yang perlu diperbaiki.</Text>
<Text >Aplikasi ini dibuat dengan harapan untuk mempermudah proses menyimpan kontak serta membantu orang-orang dalam membuat relasi baru.</Text>
<Text>Aplikasi ini dibuat oleh Muhammad Syafiq untuk menyelesaikan Tugas Akhir di Politeknik Negeri Batam.</Text>
<Text></Text>
<Text></Text>
<Text></Text>
</>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    width: '100%',
    height: '8%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#6D4C41'
  },
  text1: {
    color: '#E0F7FA',
    fontWeight: 'bold',
    fontSize: 35,
    letterSpacing: 4
  },
  textB: {
    flex: 1,
    textAlignVertical: 'center',
    color: 'black',
    fontWeight: 'normal',
    fontSize: 28,
    letterSpacing: 0,
    textAlign: 'center'
    
  }
});

import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Button,
  PermissionsAndroid,
  ToastAndroid
} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import Contacts from 'react-native-contacts';
import QRCode from 'react-native-qrcode-svg';
import {TextInput} from 'react-native-gesture-handler';
import RNFS from "react-native-fs"
import {Picker} from '@react-native-community/picker';
import { color } from 'react-native-reanimated';

export default class ContactScreen extends Component {
  static navigationOptions = {
    title: 'New Contact',
  };

  constructor(props) {
    super(props);
    const {navigation} = this.props;
    const text = navigation.getParam('text');
    const lines = text.match(/[^\r\n]+/g);

    this.state = {
      isEnable: true,
      text: text,
      lines: lines,
      //buat qr
      start: 'BEGIN:VCARD',
      ver: 'VERSION:3.0',
      nama: '',
      company: '',
      title: '',
      tel: '',
      tel1: '',
      tel2: '',
      mail: '',
      mail1: '',
      mail2: '',
      adr: '',
      end: 'END:VCARD',
      //end
      contact: {
        // familyName: null,
        // givenName: null,
        displayName: '',
        phoneNumbers: [],
        // phoneNumbers: {
        //   label: 'work',
        //   number: 234234324
        // },
        emailAddresses: [],
        company: '',
        jobTitle: '',
        postalAddresses: [],
     
      },

      lineTypes: [
        // {label: 'Nama Depan', value: 'givenName'},
        // {label: 'Nama Belakang', value: 'familyName'},
        {label: 'Nama*', value: 'displayName'},
        {label: 'Nomor*', value: 'phoneNumbers'},
      
        {label: 'E-mail', value: 'emailAddresses'},
        {label: 'Perusahaan', value: 'company'},
        {label: 'Jabatan', value: 'jobTitle'},
        {label: 'Alamat', value: 'postalAddresses'},
        {label: 'Kosong', value: ''},
      ],
    };

    this.selectedType = this.selectedType.bind(this);
  }

  selectedType(type, value) {
    
 
    var updatedContact = Object.assign({}, this.state.contact);
    
    if (type == 'phoneNumbers' ) {
      updatedContact[type].push({
        label: 'mobile',
        number: value.replace(/ /g, ''),
      });

    }  else if (type == 'phoneNumbers') {
      updatedContact[type].push({
        label: 'work',
        number: value.replace(/ /g, ''),
      });

    } 
    else if (type == 'emailAddresses') {
      updatedContact[type].push({
        label: 'work',
        email: value.replace(/ /g, ''),
      });
    } 
    else if (type == 'emailAddresses') {
      updatedContact[type].push({
        label: 'work',
        email: value.replace(/ /g, ''),
      });
    } else if (type == 'postalAddresses') {
      updatedContact[type].push({
        label: 'home',
        formattedAddress: value.replace(/ /g, ''),
        street: value.replace(/ /g, ''),
        city: value.replace(/ /g, ''),
        state: value.replace(/ /g, ''),
        country: value.replace(/ /g, ''),
        postCode: value.replace(/ /g, ''),
        region: value.replace(/ /g, ''),
      });
    } else {
      updatedContact[type] = value;
    }
    if( Object.entries(this.state.contact.phoneNumbers).length >= 4){
      alert('hanya bisa memilih 3 nomor');
      return false;

    }
    this.setState({contact: updatedContact});


  }

  saveContact() {
    let contact = this.state.contact;

    // Contacts.openContactForm(this.state.contact, (err, contact) => {
    //   if (err) throw err;
    //   // contact has been saved
    // // })
    // console.log('data kontak',this.state.contact)
console.log(contact.phoneNumbers);
 console.log(Object.entries(this.state.contact.phoneNumbers).length);

    if (contact.displayName == null || contact.phoneNumbers.length == 0) {
      alert('Pilih tipe tipe atau kosongkan');
    } else {
      Contacts.openContactForm(contact, (err) => {
        console.log(err);
      });
    }
  }
// buat simpan qr ke gallery
saveQrToDisk() {
          //
 
          try {
            const granted =  PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
              {
                title: "Storage Permission",
                message: "Aplikasi butuh izin akses penyimpanan"
              }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
              Alert.alert("Permission granted","");
            }
          } catch (err) {
            console.warn(err);
          }
        
        //
  this.svg.toDataURL((data) => {
    RNFS.writeFile(
      RNFS.DownloadDirectoryPath + '/' + this.state.nama + '.png',
      data,
      'base64',
    )
      .then((success) => {
        console.log('FILE WRITTEN!');
      })
      .then(() => {
        this.setState({busy: false, imageSaved: true});
        ToastAndroid.show('Tersimpan ke gallery !!!', ToastAndroid.SHORT);
      })
      .catch((err) => {
        console.log(err.message);
      });
  });
}
  // buat set State qr
  anuQr() {
    this.setState({nama: this.state.contact.displayName});
    this.setState({company: this.state.contact.company});
    this.setState({title: this.state.contact.jobTitle});
    if( Object.entries(this.state.contact.phoneNumbers).length >= 1){
      let objp = this.state.contact.phoneNumbers[0].number.replace(/[^0-9+]/g, '')
      this.setState({tel: objp});
      console.log('masook', this.state.tel);
    } 
    if( Object.entries(this.state.contact.phoneNumbers).length >= 2){
      let objp = this.state.contact.phoneNumbers[1].number.replace(/[^0-9+]/g, '')
      this.setState({tel1: objp});
    } 
    if( Object.entries(this.state.contact.phoneNumbers).length >= 3){
      let objp = this.state.contact.phoneNumbers[2].number.replace(/[^0-9+]/g, '')
      this.setState({tel2: objp});
    }
    if( Object.entries(this.state.contact.emailAddresses).length >= 1){
      this.setState({mail: this.state.contact.emailAddresses[0].email});
    }
    if( Object.entries(this.state.contact.emailAddresses).length >= 2){
      this.setState({mail1: this.state.contact.emailAddresses[1].email});
    }
    if( Object.entries(this.state.contact.emailAddresses).length >= 3){
      this.setState({mail2: this.state.contact.emailAddresses[2].email});
    }
     if( Object.entries(this.state.contact.postalAddresses).length >= 1){
      this.setState({adr: this.state.contact.postalAddresses[0].street});
    
    }

    // this.setState({tel: this.state.tel.replace(/[^0-9]+/, '')});
    this.setState({isEnable: false});
  }
  
  anuQr1(){
    let contact = this.state.contact;

    if (contact.displayName == null) {
      alert('Pilih Nama'); 
    }else if( Object.entries(this.state.contact.phoneNumbers).length <= 0){ 
      alert('Pilih Nomor'); 
    //  } else if( Object.entries(this.state.contact.emailAddresses).length <= 0){ 
    //     alert('Pilih Email');  
    //   } else if (contact.company == null){
    //   alert('Pilih Perusahaan'); 
    // }else if (contact.jobTitle == null){
    //   alert('Pilih Jabatan'); 
    // } else if( Object.entries(this.state.contact.postalAddresses).length <= 0){ 
    //     alert('Pilih Alamat');  
    //   
    }else {
      this.anuQr();
    }

 console.log(Object.entries(this.state.contact.phoneNumbers).length );
 console.log('nomor',this.state.tel);
 console.log('nomor1',this.state.tel1);
 console.log('nama',this.state.nama);
  }
  
  render() {
    let logoFromFile = require('./icon.png');
    // console.log('state', this.state)
    return (
      <View style={{flex: 1}}>
        <ScrollView style={styles.container}>
          <Text style={{fontWeight: 'bold'}}>
            {' '}
            Pilih Semua data yang dibutuhkan
          </Text>
          {this.state.lines.map((line, index) => {
            return (
              <>
                <View style={styles.jarak}>
                  <View style={styles.lineContainer} key={index}>
            

                    <View style={styles.lineType}>
                      <RNPickerSelect
                      
                      // selectedValue={this.state.contact.displayName}
                        placeholder={{
                          label: 'Pilih tipe atau kosongkan',
                          value: null,
                          color: 'red',
                        }}
                        onValueChange={(type) => this.selectedType(type, line)}
                        items={this.state.lineTypes}
                        style={pickerStyle}
                      />
                        </View>
                              <Text style={styles.lineText}>{line}</Text>
                  
                  </View>
                </View>
              </>
            );
          })}
          <View   style={{width: '100%', alignItems: 'center'}}>
           <Button
              color="green"
              onPress={() => {
                this.saveContact();
              }}
              title="Save Contact"
            />
            </View>
          <View
            style={{
              flexDirection: 'row',
              margin: 20,
              justifyContent: 'space-evenly',
            }}>
           

            <Button
              onPress={() => {
                this.anuQr1();
                // console.log('nama ', this.state.nama);
                // console.log('tlp ', this.state.contact.phoneNumbers[0].number);
              }}
          
              title="Set QRCode"
            />
            <Button
              onPress={() => {
                this.saveQrToDisk();
              }}
              disabled = {this.state.isEnable} 
              title="Save QRCode"
            />
          </View>
          <View style={{alignItems: 'center'}}>
            
            <QRCode
              //QR code value
              value={
                this.state.start +
                '\n' +
                this.state.ver +
                '\n' +
                // data nama
                'N:' +
                this.state.nama +
                '\n' +
                'FN:' +
                this.state.nama +
                '\n' +
                //data telpon
                'TEL:' +
                this.state.tel +
                '\n' +
                //data telpon 1
                'TEL:' +
                this.state.tel1 +
                '\n' +
                  //data telpon 2
                  'TEL:' +
                  this.state.tel2 +
                  '\n' +
                //data email
                'EMAIL:' +
                this.state.mail +
                '\n' +
                 //data email111111
                 'EMAIL:' +
                 this.state.mail1 +
                 '\n' +
                  //data email2222222
                'EMAIL:' +
                this.state.mail2 +
                '\n' +
                //data alamat
                'ADR:' +
                this.state.adr +
                '\n' +
                //data perusahaan
                'ORG:' +
                this.state.company +
                '\n' +
                //data Jabatan
                'TITLE:' +
                this.state.title +
                '\n' +
                this.state.end
              }
              size={300}
              getRef={(c) => (this.svg = c)}
              quietZone={20}
              logo={logoFromFile}
              // logoBackgroundColor="transparent"
            />
          </View>
        </ScrollView>
      </View>
    );
  }

}
const pickerStyle = {
	inputIOS: {
		color: 'white',
		paddingTop: 13,
		paddingHorizontal: 10,
		paddingBottom: 12,
	},
	inputAndroid: {
		color: 'green',
	},
	placeholderColor: 'black',
	underline: { borderTopWidth: 0 },
	icon: {
		position: 'absolute',
		backgroundColor: 'transparent',
		borderTopWidth: 5,
		borderTopColor: '#00000099',
		borderRightWidth: 5,
		borderRightColor: 'transparent',
		borderLeftWidth: 5,
		borderLeftColor: 'transparent',
		width: 0,
		height: 0,
		top: 20,
		right: 15,
	},
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  lineText: {
    padding: 3,
  },
  lineContainer: {
    flex: 1,
    borderColor: 'green',
    backgroundColor: '#E3F2FD',
    borderTopWidth: 2,
    borderBottomWidth: 2,
    borderRadius: 10,
    padding: 5,
    width: '95%',
    alignSelf: 'center',
  },
  jarak: {
    paddingVertical: 7,
  },
  lineType: {
    borderRadius: 15,
    borderWidth: 1,
  },
  picker: {
    fontSize: 10,
    borderWidth: 1,
    borderColor: 'red',
    borderRadius: 24,
    backgroundColor: 'white',
    color: 'blue',
    width: '80%',
  },
  saveButton: {
    alignContent: 'center',
    alignSelf: 'center',
    paddingVertical: 15,
    backgroundColor: 'green',
  },
  btn: {
    width: '100%',
    height: '85%',
    padding: 5,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
});

import React, { Component } from 'react';
import { StyleSheet, Dimensions, Text, View,ToastAndroid } from 'react-native';
import { RNCamera } from 'react-native-camera';
import {Icon} from 'react-native-elements';

export default class CameraScreen extends Component {
  takePicture = async function() {
    if (this.camera) {
      const options = { quality: 0.5, base64: true };
      const data = await this.camera.takePictureAsync(options)
      this.detectText(data.base64)
    }
    ToastAndroid.show('Tunggu ....', ToastAndroid.SHORT);
  };
  detectText(base64){
    fetch("https://vision.googleapis.com/v1/images:annotate?key=" + "AIzaSyB7VqdQ-ComwMnzg_BCHlQ0jx0Z4ErBe50", {
        method: 'POST',
        body: JSON.stringify({
          "requests": [{
            "image": { "content": base64 },
            "features": [
                { "type": "TEXT_DETECTION" }
            ]}]
      })
    })
    .then(response => { return response.json() })


    .then(jsonRes => {
      console.log('Image was loaded!')
      ToastAndroid.show('Selesai !!!', ToastAndroid.SHORT);
      let text = jsonRes.responses[0].fullTextAnnotation.text
      this.props.navigation.navigate('ContactScreen', { text: text })
    }).catch(err => {
      console.log('Error', err)
      ToastAndroid.show('Network fail, please retake picture !!!', ToastAndroid.SHORT);
    })
  }

  render() {
    return (
      <View style={styles.container}>
        <RNCamera
          ref={cam => { this.camera = cam }}
          style={styles.preview}
          >
            
            <Text style={styles.TextCam}>Aplikasi hanya mencatat teks pada kartu nama !</Text>
            <Icon
              raised
              name="camera"
              type="antdesign"
              size={50}
              color="#3949AB"
              
              onPress={this.takePicture.bind(this)}
            />
<Text> </Text>

        </RNCamera>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  TextCam: {
flex: 1,
paddingTop: 20,
fontSize: 15,
fontWeight: 'bold',
color: 'yellow',
textAlign: 'center'

  },
  preview: { 
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    color: '#000',
    padding: 10,
    margin: 40
  }
});
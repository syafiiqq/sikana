import React from 'react';
import {
  SafeAreaView,
  PermissionsAndroid,
  Alert,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  TextInput,
  ToastAndroid,
  ViewPropTypes,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import QRCode from 'react-native-qrcode-svg';

import RNFS from 'react-native-fs';
// var start = 'BEGIN:VCARD';
// var ver = 'VERSION:3.0';
// var nama = 'N:Muhammad';
// var company = 'ORG:student_co';
// var tel = 'TEL:082285804776';
// var mail = 'EMAIL:user1@mail.com';
// var adr = 'ADR:Pemda 2, Jalan Pulau Batang no 189';
// var end = 'END:VCARD';

export default class GenQr extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      textInputN: [],
      inputDataN: [],
      textInputE: [],
      inputDataE: [],
      isEnable: true,
      AddisEnable: false,
      inputValue: '',
      // Default Value buat textInputN
      ValueQr: '',
      // value buat qrnya
      start: 'BEGIN:VCARD',
      ver: 'VERSION:3.0',

      nama: '',
      nama1: '',

      company: '',
      company1: '',

      title: '',
      title1: '',

      tel: '',
      tel1: '',

      tel11: '',
      tel22: '',
      tel33: '',

      mail: '',
      mail1: '',

      mail11: '',
      mail22: '',
      mail33: '',

      adr: '',
      adr1: '',

      end: 'END:VCARD',
    };
  }

  handleNama = (text) => {
    this.setState({nama: text});
  };
  handlePerusahaan = (text) => {
    this.setState({company: text});
  };
  handleTitle = (text) => {
    this.setState({title: text});
  };
  handleTel = (text) => {
    // this.setState({tel: text.replace(/[^0-9]/g, '')});
    if (/^[0-9]*[+]?[0-9]*$/.test(text.toString())) { 
      this.setState({ tel: text });
    }
    // var regex = /[^0-9]/;
    // if (text.match(regex)) {
    //   alert('Harus memasukkan angka');
      
    //   return false;
    // }
  };
  handleMail = (text) => {
    this.setState({mail: text});
  };
  handleAdr = (text) => {
    this.setState({adr: text});
  };
  saveQrToDisk() {
    //

    try {
      const granted = PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'Storage Permission',
          message: 'Aplikasi butuh izin akses penyimpanan',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        Alert.alert('Permission granted', '');
      }
    } catch (err) {
      console.warn(err);
    }

    //
    this.svg.toDataURL((data) => {
      RNFS.writeFile(
        RNFS.DownloadDirectoryPath + '/' + this.state.nama + '.png',
        data,
        'base64',
      )
        .then((success) => {
          console.log('File berhasil dibuat');
        })
        .then(() => {
          this.setState({busy: false, imageSaved: true});
          ToastAndroid.show('Saved to gallery !!', ToastAndroid.SHORT);
        })
        .catch((err) => {
          console.log(err.message);
        });
    });
  }
  getValueQr() {
    if (this.state.nama == null || this.state.tel.length == 0) {
      alert('Isi data, minimal nama dan nomor');
      this.setState({isEnable: true});
    } else {
      this.setState({isEnable: false});
    }

    this.setState({nama1: this.state.nama});
    this.setState({company1: this.state.company});
    this.setState({title1: this.state.title});
    /////////
    this.setState({tel1: this.state.tel});
    this.setState({mail1: this.state.mail});
let cobaN =  this.state.inputDataN;
let cobaE =  this.state.inputDataE;
//     if (this.state.textInputN.length == null) {

//       this.setState({coba : null});
//       this.setState({tel11: coba});
//       this.setState({tel22: ""});
//       this.setState({tel22: ""});
//     }



    if (this.state.textInputN.length  ==  0) {
      // this.setState({ cobaN : null});
      // this.setState({tel11: cobaN});
      this.setState({tel11: ''});
      this.setState({tel22: ''});
      console.log('masok0');
     
    } 
   
    if (this.state.textInputN.length  == 1 ) {
      console.log('count',this.state.textInputN.length );
      console.log('masok1'); 
    
      this.setState({tel11: this.state.inputDataN[0].text});
      
    }
    if (this.state.textInputN.length  >= 1) {
      this.setState({tel11: this.state.inputDataN[0].text});
    
    }
    if (this.state.textInputN.length == 2) {
      this.setState({tel22: this.state.inputDataN[1].text});
      console.log('masok2');
    } 
  
//////////////////////// buat email addddd

if (this.state.textInputE.length  ==  0) {
  // this.setState({ cobaN : null});
  // this.setState({tel11: cobaN});
  this.setState({mail11: ''});
  this.setState({mail22: ''});
  console.log('EEEEmasok0');
 
} 
if (this.state.textInputE.length  == 1 ) {
  console.log('count',this.state.textInputE.length );
  console.log('EEEEEmasok1'); 

  this.setState({mail11: this.state.inputDataE[0].text});
}
if (this.state.textInputE.length  >= 1) {
  this.setState({mail11: this.state.inputDataE[0].text});

}
if (this.state.textInputE.length >= 2) {
  this.setState({mail22: this.state.inputDataE[1].text});
} 

//     //////////////////
console.log("tel1 ==", this.state.tel1)
console.log("tel11 --", this.state.tel11)
console.log("tel22 --", this.state.tel22)
console.log('N length',this.state.textInputN.length)
console.log('\n'+ '===========================')
    ////////////////////////
    console.log("mail1 ==", this.state.mail1)
    console.log("mail11 --", this.state.mail11)
    console.log("mail22 --", this.state.mail22)
    console.log('E length',this.state.textInputE.length)
    console.log('\n'+ '+++++++++++++++++++++++++++')
    this.setState({adr1: this.state.adr});

    
  }
  // function buat add textInputN Nomor
  addtextInputN = (index) => {
    let textInputN = this.state.textInputN;


    let count = this.state.textInputN.length;

    if (this.state.tel.length < 7) {
      alert('Masukkan Dulu nomor pada kolom yang kosong');
      return false;
    }
    
  
    if (count >= 2) {
      alert('Hanya bisa memasukkan 3 nomor');
      return false;
    }
    textInputN.push(
      <View
        style={{
          flexDirection: 'row',
          margin: 2,
          justifyContent: 'space-evenly',
        }}>
        <View style={styles.inputViewAdd}>
          <TextInput
            style={styles.inputText}
            onChangeText={(text) => this.addValuesN(text, index)}
            placeholder={'No ' + (this.state.textInputN.length + 1)}
            keyboardType={'phone-pad'}
          />
        </View>
        <View style={{marginHorizontal: 10}}>
          <Button
            color="red"
            title="-"
            onPress={() => this.removetextInputN()}
          />
        </View>
      </View>,
    );

    this.setState({textInputN});
  };
  // end function buat add textInputN Nomor

  // function buat add textInputN Email
  addtextInputE = (index) => {
    let textInputE = this.state.textInputE;


    let count = this.state.textInputE.length;

    if (this.state.mail.length < 4) {
      alert('Masukkan Dulu Email pada kolom ini');
      return false;
    }
    if (count >= 2) {
      alert('Hanya bisa memasukkan 3 Email');
      return false;
    }
    textInputE.push(
      <View
        style={{
          flexDirection: 'row',
          margin: 2,
          justifyContent: 'space-evenly',
        }}>
        <View style={styles.inputViewAdd}>
          <TextInput
            style={styles.inputText}
            onChangeText={(text) => this.addValuesE(text, index)}
            placeholder={'No ' + (this.state.textInputE.length + 1)}
            keyboardType={'email-address'}
          />
        </View>
        <View style={{marginHorizontal: 10}}>
          <Button
            color="red"
            title="-"
            onPress={() => this.removetextInputE()}
          />
        </View>
      </View>,
    );

    this.setState({textInputE});
  };
  // end function buat add textInputN Email
  ////////////// //////////////////////////////////////////////////////////////////////////////

  // function hapus textInputN Nomor

  removetextInputN = () => {
    let count = this.state.textInputN.length;
    if (count <= 2) {
      this.setState({AddisEnable: false});
    }
    let textInputN = this.state.textInputN;
    let inputDataN = this.state.inputDataN;
    textInputN.pop();
    inputDataN.pop();
    this.setState({textInputN, inputDataN});
  };
  // end function hapus textInputN Nomor

  // function hapus textInputN Email
  removetextInputE = () => {
    let textInputE = this.state.textInputE;
    let inputDataE = this.state.inputDataE;
    textInputE.pop();
    inputDataE.pop();
    this.setState({textInputE, inputDataE});
  };
  // end function hapus textInputN Email

  // function to add text from textInputNs into single array Nomor
  addValuesN = (text, index) => {
    let dataArray = this.state.inputDataN;
    let checkBool = false;
    if (dataArray.length !== 0) {
      dataArray.forEach((element) => {
        if (element.index === index) {
          element.text = text;
          checkBool = true;
        }
      });
    }
    if (checkBool) {
      this.setState({
        inputDataN: dataArray,
      });
    } else {
      dataArray.push({text: text, index: index});
      this.setState({
        inputDataN: dataArray,
      });
    }
  };
  // end function to add text from textInputNs into single array Nomor

  //function to add text from textInputNs into single array Email
  addValuesE = (text, index) => {
    let dataArray = this.state.inputDataE;
    let checkBool = false;
    if (dataArray.length !== 0) {
      dataArray.forEach((element) => {
        if (element.index === index) {
          element.text = text;
          checkBool = true;
        }
      });
    }
    if (checkBool) {
      this.setState({
        inputDataE: dataArray,
      });
    } else {
      dataArray.push({text: text, index: index});
      this.setState({
        inputDataE: dataArray,
      });
    }
  };
  // end function to add text from textInputNs into single array Email

  //function to console the output
  getValues = () => {
    console.log('DataN', this.state.inputDataN);
    console.log('DataN0', this.state.textInputN.length);
    console.log('DataN1', this.state.inputDataN[0].text);
    // console.log('DataN2', this.state.inputDataN[2].text);

    // console.log('DataE', this.state.inputDataE);
    // console.log('DataE0', this.state.inputDataE[0].text);
    // console.log('DataE1', this.state.inputDataE[1].text);
    // console.log('DataE2', this.state.inputDataE[2].text);
    // console.log('adasdasd',this.state.textInputN.length);
  };
  //////////////////////////////////////////////////////

  render() {
    let logoFromFile = require('../fungsi1/icon.png');
    return (
      <>
        <ScrollView>
          <Text style={styles.textAtas}>
            Masukkan data yang dibutuhkan untuk membuat QRCode{' '}
          </Text>
          <View style={styles.centerForm}>
            <View style={styles.inputView}>
              <TextInput
                style={styles.inputText}
                placeholder="Nama*"
                placeholderTextColor="#003f5c"
                onChangeText={this.handleNama}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                margin: 0,
                justifyContent: 'space-evenly',
              }}>
              <View style={{marginHorizontal: 3}}>
                <Button
                  color="green"
                  title="+"
                  onPress={() =>
                    this.addtextInputN(this.state.textInputN.length)
                  }
                  disabled={this.state.AddisEnable}
                />
              </View>
              <View style={styles.inputViewNoEm}>
                <TextInput
                  style={styles.inputText}
                  placeholderTextColor="#003f5c"
                  placeholder="No Tel*"
                  onChangeText={this.handleTel}
                  value = {this.state.tel}
                  keyboardType={'phone-pad'}
                />
              </View>
              {/* <View style={{marginHorizontal:10}}>
              <Button color="red" title="-" onPress={() => this.removeTextInput()} />
              </View> */}
            </View>
            {this.state.textInputN.map((value) => {
              return value;
            })}

            {/* <Button title="Get Values" onPress={() => this.getValues()} /> */}

            <View
              style={{
                flexDirection: 'row',
                margin: 0,
                justifyContent: 'space-evenly',
              }}>
              <View style={{marginHorizontal: 3}}>
                <Button
                  color="green"
                  title="+"
                  onPress={() =>
                    this.addtextInputE(this.state.textInputE.length)
                  }
                  disabled={this.state.AddisEnable}
                />
              </View>
              <View style={styles.inputViewNoEm}>
                <TextInput
                  style={styles.inputText}
                  placeholderTextColor="#003f5c"
                  placeholder="email"
                  onChangeText={this.handleMail}
                  keyboardType={'email-address'}
                />
              </View>
            </View>
            {this.state.textInputE.map((value) => {
              return value;
            })}

            <View style={styles.inputView}>
              <TextInput
                style={styles.inputText}
                placeholderTextColor="#003f5c"
                placeholder="Perusahaan"
                onChangeText={this.handlePerusahaan}
              />
            </View>
            <View style={styles.inputView}>
              <TextInput
                style={styles.inputText}
                placeholderTextColor="#003f5c"
                placeholder="Jabatan"
                onChangeText={this.handleTitle}
              />
            </View>
            <View style={styles.inputView}>
              <TextInput
                style={styles.inputText}
                placeholderTextColor="#003f5c"
                placeholder="Alamat"
                onChangeText={this.handleAdr}
              />
            </View>
          {/* </View> */}
          <View style={{width: '100%', alignItems: 'center'}}>
            <Button
              onPress={() => {
                this.getValueQr();
              }}
              title="Set QRCode"
            />
            <Text> </Text>
            <Button
              onPress={() => {
                // onPress={this.getValueQr}
                console.log('Nama', this.state.nama);
                console.log('email', this.state.mail);
                console.log('alamat', this.state.adr);
                console.log('nomor', this.state.tel);
                console.log('perusahaan', this.state.company);
                this.saveQrToDisk();
              }}
              disabled={this.state.isEnable}
              title="Save QRCode"
            />
          </View>
          <Text></Text>

          <View style={styles.center}>
            <QRCode
              //QR code value
              value={
                this.state.start +
                '\n' +
                this.state.ver +
                '\n' +
                // data nama
                'N:' +
                this.state.nama1 +
                '\n' +
                'FN:' +
                this.state.nama1 +
                '\n' +
                //data telpon
                'TEL:' +
                this.state.tel1 +
                '\n' +

                //data telpon1111111111111
                'TEL:' +
                this.state.tel11 +
                '\n' +
                //data telpon2222222222
                'TEL:' +
                this.state.tel22 +
                '\n' +
       

                //data email
                'EMAIL:' +
                this.state.mail1 +
                '\n' +

                //data email11111111111111111
                'EMAIL:' +
                this.state.mail11 +
                '\n' +
                //data email222222222222
                'EMAIL:' +
                this.state.mail22 +
                '\n' +
                // //data email3333333333
                // 'EMAIL:' +
                // this.state.mail33 +
                // '\n' +

                //data alamat
                'ADR:' +
                this.state.adr1 +
                '\n' +
                //data perusahaan
                'ORG:' +
                this.state.company1 +
                '\n' +
                //data Jabatan
                'TITLE:' +
                this.state.title1 +
                '\n' +
                this.state.end
              }
              size={300}
              getRef={(c) => (this.svg = c)}
              quietZone={20}
              logo={logoFromFile}
              logoBackgroundColor="transparent"
            />
            {/* <QRCode
            styles={styles.tengah}
            value={kontak1.start+'\n'+ver+'\n'+nama+'\n'+company+'\n'+tel+'\n'+mail+'\n'+adr+'\n'+end}
            size={300}
          /> */}
          </View>
          </View>
        </ScrollView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  center: {
    alignItems: 'center',
    paddingTop: 10,
  },
  textAtas: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 14,
    paddingTop: 10,
    padding: 5,
    textAlign: 'center',
  },
  centerForm: {
    alignItems: 'center',
    paddingTop: 10,
  },
  inputView: {
    width: '95%',
    backgroundColor: '#E0F7FA',
    borderRadius: 25,
    height: 40,
    marginBottom: 10,
    justifyContent: 'center',
    padding: 10,
    borderWidth: 1.0,
  },
  inputViewNoEm: {
    width: '85%',
    backgroundColor: '#E0F7FA',
    borderRadius: 25,
    height: 40,
    marginBottom: 10,
    justifyContent: 'center',
    padding: 10,
    borderWidth: 1.0,
  },
  inputViewAdd: {
    width: '85%',
    backgroundColor: '#B2DFDB',
    borderRadius: 25,
    borderColor: 'green',
    height: 40,
    marginBottom: 10,
    justifyContent: 'center',
    padding: 10,
    borderWidth: 1.0,
  },
  inputText: {
    height: 50,
    color: 'green',
  },
  textInput: {
    height: 40,
    borderColor: 'black',
    borderWidth: 1,
    margin: 20,
  },
});

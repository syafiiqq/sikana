import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import ContactHome from './ContactHome';

export default class DetailContact extends Component {
  render() {
  return (
    <>
<View style={styles.header}>
  <Text style={styles.text1}>DETAIL CONTACT</Text>
</View>
<Text> </Text>

    </>
  );
}
}

const styles = StyleSheet.create({
  header: {
    width: '100%',
    height: '8%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#0288D1'
  },
  text1: {
    color: '#E0F7FA',
    fontWeight: 'bold',
    fontSize: 35,
    letterSpacing: 0
    
  }
});
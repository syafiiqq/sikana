
//Import React
import React, {useState, useEffect} from "react";

//Import all required component
import {
  PermissionsAndroid,
  Platform,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  FlatList,
  TextInput,
  Alert,
  TouchableHighlight,
  Modal,
} from "react-native";

import Contacts from "react-native-contacts";
import ListItem from "./ListItem";

const ContactHome = () => {
  let [kontak = {
    nomor: []
  }
    
    , setKontak] = useState([]);
  let [angka, setNomor] = useState([]);
  let [contacts, setContacts] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);

 var simpanC =  {
    perusahaan :kontak.company,
    "department":null,
    nama: kontak.displayName,
    "emailAddresses":[
       {
          "email":"syafiiqq@gmail.com",
          "id":"5292",
          "label":"work"
       }
    ],
    "familyName":"SYAFIQ",
    "givenName":"MUHAMMAD",
    "hasThumbnail":false,
    "jobTitle":"Student",
    "middleName":"",
    "note":"",
  //  nomor: kontak.phoneNumbers[0].number,
    "postalAddresses":[
       {
          "city":"OPemda2,JalanPulauBatang,no189",
          "country":"OPemda2,JalanPulauBatang,no189",
          "formattedAddress":"Pemda2,JalanPulauBatang,no189",
          "label":"work",
          "postCode":"OPemda2,JalanPulauBatang,no189",
          "region":"OPemda2,JalanPulauBatang,no189",
          "state":"OPemda2,JalanPulauBatang,no189",
          "street":"OPemda2,JalanPulauBatang,no189"
       }
    ],
    "prefix":null,
    "rawContactId":"1687",
    "recordID":"1689",
    "suffix":null,
    "thumbnailPath":"",
    "urlAddresses":[
 
    ]
 }

  useEffect(() => {
    if (Platform.OS === "android") {
      PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
        title: "Contacts",
        message: "This app would like to view your contacts."
      }).then(() => {
        loadContacts();
      });
    } else {
      loadContacts();
    }
  }, []);

  const loadContacts = () => {
    Contacts.getAll((err, contacts) => {
        console.log(contacts.length)
    contacts.sort((A, B) => A.givenName > B.givenName);
    
    //   console.log('contacts -> ', contacts);
      if (err === "denied") {
        alert("Permission to access contacts was denied");
        console.warn("Permission to access contacts was denied");
      } else {
        setContacts(contacts);
        // console.log('contacts', contacts);
      }
    });
  }

  const search = (text) => {
    const phoneNumberRegex = /\b[\+]?[(]?[0-9]{2,6}[)]?[-\s\.]?[-\s\/\.0-9]{3,15}\b/m;
    if (text === "" || text === null) {
      loadContacts();
    } else if (phoneNumberRegex.test(text)) {
      Contacts.getContactsByPhoneNumber(text, (err, contacts) => {
        contacts.sort((a, b) => a.givenName.toLowerCase() > b.givenName.toLowerCase());
        setContacts(contacts);
     
        // console.log('contacts', contacts);
      });
    } else {
      Contacts.getContactsMatchingString(text, (err, contacts) => {
        contacts.sort((a, b) => a.givenName.toLowerCase() > b.givenName.toLowerCase());
        setContacts(contacts);
       
        // console.log('contacts', contacts);
      });
    }
  }
// menu buat ke detail kontak

const openContact = (contact) => {
  
  // var x =  JSON.parse(contact);
 var kontak =  contact;
  setKontak(x);
  var x = JSON.stringify(kontak);

  console.log("data" ,  kontak);
//   setNomor(hape);
//   console.log('nama', obj,'\n')
//   // console.log('\n data=>>>>>', x);
//   let y = contact.displayName;
// var hape = contact.phoneNumbers[0].number;
// var obj = y.toString();

// var haha = x.toString();

  // Contacts.updateContact(contact, () => {})
              setModalVisible(true); 
 
};
// const KeDetail = (contact) => {
//   this.props.navigation.navigate('DetContact')
// };
return (
  <SafeAreaView style={styles.container}>
    
    <View style={styles.centeredView}>
    <Modal
      animationType="slide"
      transparent={true}
      visible={modalVisible}
      onRequestClose={() => {
        Alert.alert("Modal has been closed.");
      }}
    >
      
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
    <Text style={styles.modalText}>
      Nama  : {simpanC.nama} {'\n'} 
      {/* Nomor : {JSON.stringify(kontak.phoneNumbers[0].number)} {'\n'} */}
      Perusahaan  : {simpanC.perusahaan} 
      </Text>

          <TouchableHighlight
            style={{ ...styles.openButton, backgroundColor: "#2196F3" }}
            onPress={() => {
              setModalVisible(!modalVisible);
            }}
          >
            <Text style={styles.textStyle}>Hide Modal </Text>
          </TouchableHighlight>
        </View>
      </View>
       </Modal>
       {/* <TouchableHighlight
      style={styles.openButton}
      onPress={() => {
        setModalVisible(true);
      }}
    >
      <Text style={styles.textStyle}>Show Modal</Text>
    </TouchableHighlight> */}
    <View style={styles.container}>
      <Text style={styles.header}>
        Access Contact List in React Native
      </Text>
      <TextInput
        onChangeText={search}
        placeholder='Search'
        style={styles.searchBar}
      />
      <FlatList
        data={contacts}
        renderItem={(contact) => {
          // {console.log('contact -> ' + JSON.stringify(contact))}
          return (<ListItem
            key={contact.item.recordID}
            item={contact.item}
            onPress={openContact}
            
          />)
        }}
        keyExtractor={item => item.recordID}
      />
    </View>
    </View>
  </SafeAreaView>
);
}
export default ContactHome;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  header: {
    backgroundColor: '#4591ed',
    color:'white',
    paddingHorizontal: 15,
    paddingVertical: 15,
    fontSize: 20
  },
  searchBar: {
    backgroundColor: '#f0eded',
    paddingHorizontal: 30,
    paddingVertical: (Platform.OS === "android") ? undefined: 15,
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }
});
import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,TouchableHighlight
} from 'react-native';
import {Icon} from 'react-native-elements';

export default class Kotak extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.kotak1}>
          <View style={styles.dalam1}>
            <Icon
              raised
              name="vcard-o"
              type="font-awesome"
              size={90}
              color="#4CAF50"
            />
            <Text style={styles.tulisan1}>Scan Kartu Nama</Text>
          </View>
        </View>
        <View style={styles.kotak}>
          <View style={styles.dalam2}>
          <Icon
              raised
              name="scan1"
              type="antdesign"
              size={50}
              color="#3949AB"
            />
            <Text style={styles.tulisan}>Scan Qrcode</Text>
          </View>
        </View>

        <View style={styles.kotak}>
          <View style={styles.dalam3}>
          <Icon
              raised
              name="qrcode"
              type="antdesign"
              size={50}
              color="#00796B"
            />
            <Text style={styles.tulisan}>Convert Kontak</Text>
          </View>
        </View>

        <View style={styles.kotak4}>
          <View style={styles.dalam4}>
          <TouchableHighlight
              onPress={() => this.props.navigation.navigate('Tentang')}>
            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
         
              <Icon name="md-contact" type="ionicon" size={50} color="white" />
              <Text style={styles.tulisan}> About Me</Text>
             
            </View>
       </TouchableHighlight>
          </View>
          
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '85%',
    padding: 5,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  kotak: {
    width: '50%',
    height: '45%',
    padding: 5,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
  },
  kotak1: {
    width: '100%',
    height: '50%',
    padding: 5,
  },
  kotak4: {
    width: '100%',
    height: '12%',
    padding: 5,
  },
  dalam1: {
    flex: 1,
    backgroundColor: '#4CAF50',
    alignItems: 'center',
    justifyContent: 'center',

    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
  },
  dalam2: {
    flex: 1,
    backgroundColor: '#3949AB',
    alignItems: 'center',
    justifyContent: 'center',
  },
  dalam3: {
    flex: 1,
    backgroundColor: '#00796B',
    alignItems: 'center',
    justifyContent: 'center',
  },
  dalam4: {
    flex: 1,
    backgroundColor: '#6D4C41',
    alignItems: 'center',
    justifyContent: 'center',

    borderBottomLeftRadius: 25,
    borderBottomRightRadius: 25,
  },
  tulisan1: {
    color: '#E0F7FA',
    fontWeight: 'normal',
    fontFamily: "vincHand",
    fontSize: 45,
  },
  tulisan: {
    color: '#E0F7FA',
    fontWeight: 'normal',
    fontFamily: "vincHand",
    fontSize: 25,
  }

});

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';


export default class Header extends React.Component {
  render() {
  return (
    <>
<View style={styles.header}>
  <Text style={styles.text1}>SIKANA</Text>

</View>
    </>
  );
}
}

const styles = StyleSheet.create({
  header: {
    width: '100%',
    height: '8%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#0288D1'
  },
  text1: {
    color: '#E0F7FA',
    fontWeight: 'bold',
    fontSize: 35,
    letterSpacing: 4
    
  }
});
import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

// new lib
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

// new screen
import HomeScreen from './screen/Homescreen';
import AboutScreen from './screen/Aboutscreen';
import CameraScreen from './screen/fungsi1/Camerascreen';
import ContactScreen from './screen/fungsi1/Contactscreen';
import ContactHome from './screen/fungsi3/ContactHome';
import ContactDetails from './screen/fungsi3/ListItem';
import Scanscreen from './screen/fungsi2/Scanscreen';
import DetailContact from './screen/fungsi3/DetailContact';
import GenQr from './screen/fungsi3/GenQr';
// end

const AppNavigator = createStackNavigator(
  {
    Home: {screen: HomeScreen},
    About: {screen: AboutScreen},
    ContactScreen : { screen: ContactScreen},
    CameraScreen: { screen: CameraScreen},
    ConHome: { screen: ContactHome },
    ConDetails: { screen: ContactDetails },
    ScanScreen: {screen : Scanscreen},
    GenQr: {screen : GenQr},
    DetContact: {screen : DetailContact}
  },
  {
    headerMode: 'none',
  },
);

const AppContainer = createAppContainer(AppNavigator);

export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
